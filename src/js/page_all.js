$(document).ready(function () {
    var is_agency = location.search.split('aff=')[1];
    if(is_agency){
        setCookie('is_agency', is_agency, 30*24*60*60*1000);
    }
    $('#suggestionsList').fadeOut();
});

function dangky_do(){
    var is_agency = getCookie('is_agency');
    if(!is_agency){
        is_agency = 0;
    }
    var full_name = $("#full_name").val();
    var phone = $("#phone").val();
    var email = $("#email").val();
    var code_agent = $("#code_agent").val();
    var password = $("#pass").val();
    var confirm = $("#confirm").val();
    $(".dk_alert").text('');
    var res = true;
    if(!full_name){
        $("#alert_full_name").text('ChÆ°a Ä‘iá»n Há» vĂ  tĂªn');
        res = false;
    }
    if(!phone){
        $("#alert_phone").text('ChÆ°a Ä‘iá»n sá»‘ Ä‘iá»‡n thoáº¡i');
        res = false;
    }
    if(!email){
        $("#alert_email").text('ChÆ°a Ä‘iá»n email');
        res = false;
    }
    if(!password){
        $("#alert_pass").text('ChÆ°a Ä‘iá»n máº­t kháº©u');
        res = false;
    }
    if(confirm != password){
        $("#alert_cpass").text('XĂ¡c nháº­n máº­t kháº©u khĂ´ng Ä‘Ăºng');
        res = false;
    }
    if(!res){
        return false;
    }
    $.post("/dang-ky-do",
        {full_name: full_name, phone: phone, email: email, password: password,r_password: confirm,is_agency: is_agency,code_agent:code_agent},
        function (res, status) {
            var data = jQuery.parseJSON(res);
            if(data.error != 0){
                $('#dk_alert').modal('show');
                $('#dk_res').text(data.msg);
            } else {
                setCookie('user_hash', data.data, 60*60*24);
                location.reload();
            }
        });
}

function user_login(){
    var username = $("#username").val();
    var password = $("#pass").val();
    $(".dk_alert").text('');
    var res = true;
    if(!username){
        $("#alert_username").text('ChÆ°a Ä‘iá»n tĂ i khoáº£n');
        res = false;
    } else {
        $("#alert_username").text('');
    }
    if(!password){
        $("#alert_pass").text('ChÆ°a Ä‘iá»n máº­t kháº©u');
        res = false;
    } else {
        $("#alert_pass").text('');
    }
    if(!res){
        return false;
    }
    $.post("/dang-nhap-do",
        {username: username, password: password},
        function (res, status) {
            var data = jQuery.parseJSON(res);
            if(data.error != 0){
                $('#dk_res').text(data.msg);
            } else {
                setCookie('user_hash', data.data, 60*60*24);
                location.reload();
            }
        });
}

function setCookie(cname, cvalue, time) {
    var d = new Date();
    // d.setTime(d.getTime() + (exdays*24*60*60*1000));
    d.setTime(d.getTime() + (time*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function delete_cookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function logout(){
    $.post("/logout", {},
        function (res, status) {
            var data = jQuery.parseJSON(res);
            if(data.error != 0){
                console.log(data);
            } else {
                delete_cookie('user_hash');
                location.reload();
            }
        });
}

function send_otp( action = ''){
    $("#bt_send_otp").addClass('v-btn--disabled');
    $("#bt_send_otp").attr("disabled", "disabled");
    countdown();
    var phone = $("#input-username").val();
    $.post("/send-otp", {phone: phone, action: action},function (res, status) {});
}
function countdown() {
    var timeleft = 30;
    var downloadTimer = setInterval(function(){
        if(timeleft < 0){
            $("#bt_send_otp").removeClass('v-btn--disabled');
            $("#bt_send_otp").removeAttr("disabled");
            document.getElementById("countdown").innerHTML = "";
            clearInterval(downloadTimer);
        } else {
            document.getElementById("countdown").innerHTML = " ( " + timeleft + " )";
        }
        timeleft -= 1;
    }, 1000);
}

function reset_pass(){
    var oldpass = $("#oldpass").val();
    var new_password = $("#new_password").val();
    var confirmPassword = $("#confirmPassword").val();
    $(".dk_alert").text('');
    var res = true;
    if(!oldpass){
        $("#alert_oldpass").text('ChÆ°a Ä‘iá»n máº­t kháº©u hiá»‡n táº¡i');
        res = false;
    } else {
        $("#alert_oldpass").text('');
    }
    if(!new_password){
        $("#alert_new_password").text('ChÆ°a Ä‘iá»n máº­t kháº©u má»›i');
        res = false;
    } else {
        $("#alert_new_password").text('');
    }
    if(confirmPassword != new_password){
        $("#alert_confirmPassword").text('XĂ¡ch nháº­n máº­t kháº©u khĂ´ng Ä‘Ăºng');
        res = false;
    } else {
        $("#alert_confirmPassword").text('');
    }
    if(!res){
        return false;
    }
    $.post("/doi-mat-khau",
        {oldpass: oldpass, new_password: new_password, confirmPassword: confirmPassword},
        function (res, status) {
            var data = jQuery.parseJSON(res);
            $('#dk_alert').modal('show');
            $('#dk_res').text(data.msg);
            setTimeout(function () {
                location.reload();
            }, 2000);
        });
}

function fogot_pass(type_send = false){
    var phone = $("#phone").val();
    $(".dk_alert").text('');
    var res = true;
    if(!phone){
        $("#alert_phone").text('ChÆ°a Ä‘iá»n sá»‘ Ä‘iá»‡n thoáº¡i');
        res = false;
    } else {
        $("#alert_phone").text('');
    }
    if(!res){
        return false;
    }
    $.post("/send-otp",
        {phone: phone, action: 'fogot_pass'},
        function (res, status) {
            var data = jQuery.parseJSON(res);
            $('#dk_alert').modal('show');
            $('#dk_res').text(data.msg);
            if(data.error != 0){
                return false;
            }
            setTimeout(function () {
                if(type_send){
                    window.location.href = '/kich-hoat-tai-khoan?username='+phone;
                }else {
                    window.location.href = '/lay-lai-mat-khau?username='+phone;
                }
            }, 2000);
        });
}


function update_pass(){
    var phone = $("#input-username").val();
    var pass = $("#pass").val();
    var confirmPassword = $("#confirm").val();
    var otp = $("#otp").val();
    $(".dk_alert").text('');
    var res = true;
    if(!phone){
        $("#alert_phone").text('ChÆ°a Ä‘iá»n sá»‘ Ä‘iá»‡n thoáº¡i');
        res = false;
    } else {
        $("#alert_phone").text('');
    }
    var res = true;
    if(!pass){
        $("#alert_pass").text('ChÆ°a Ä‘iá»n máº­t kháº©u');
        res = false;
    } else {
        $("#alert_pass").text('');
    }
    if(confirmPassword != pass){
        $("#alert_cpass").text('XĂ¡ch nháº­n máº­t kháº©u khĂ´ng Ä‘Ăºng');
        res = false;
    } else {
        $("#alert_cpass").text('');
    }
    if(!otp){
        $("#alert_otp").text('ChÆ°a Ä‘iá»n mĂ£ xĂ¡c nháº­n');
        res = false;
    } else {
        $("#alert_otp").text('');
    }
    if(!res){
        return false;
    }
    $.post("/update-pass", {phone: phone,pass: pass,confirmPassword: confirmPassword,otp: otp},
        function (res, status) {
            var data = jQuery.parseJSON(res);
            $('#dk_alert').modal('show');
            $('#dk_res').text(data.msg);
            if(data.error == 0){
                setTimeout(function () {
                    window.location.href = data.data;
                }, 2000);
            }
        });
}

function getPrdType(ProdType){
    $(".v-tabs-bar__content").find(".v-tab--active").removeClass('v-tab--active');
    $("#tab-prod-" + ProdType).addClass("v-tab--active");
    if(ProdType == 3 || ProdType == 4){
        $("#note_info_text").text('Sá»‘ Ä‘iá»‡n thoáº¡i náº¡p (Vui lĂ²ng nháº­p sá»‘ Ä‘iá»‡n thoáº¡i 10 sá»‘, báº¯t Ä‘áº§u tá»« sá»‘ 0, vĂ­ dá»¥: 09...) *');
        $("#note_info_input").text('Sá»‘ Ä‘iá»‡n thoáº¡i náº¡p *');
        $("#input-email-by").attr("placeholder", "Vui lĂ²ng nháº­p sá»‘ Ä‘iá»‡n thoáº¡i muá»‘n náº¡p");
    } else {
        $("#note_info_text").text('Äá» nghá»‹ quĂ½ khĂ¡ch Ä‘iá»n chĂ­nh xĂ¡c Ä‘á»‹a chá»‰ email Ä‘á»ƒ trĂ¡nh nháº§m láº«n vĂ  máº¥t tháº».');
        $("#note_info_input").text('Email nháº­n mĂ£ tháº» *');
        $("#input-email-by").attr("placeholder", "Vui lĂ²ng nháº­p email nháº­n mĂ£ tháº»");
    }
    get_provider(ProdType);
}

function NapType(type_nap){
    $(".v-tabs-bar__content").find(".tab_nap_tien_active").removeClass('tab_nap_tien_active');
    $("#tab-nap-" + type_nap).addClass("tab_nap_tien_active");
    if(type_nap == 1){
        $("#from_nap_cknh").show();
        $("#from_nap_ATM").hide();
    } else {
        $("#from_nap_cknh").hide();
        $("#from_nap_ATM").show();
    }
}

function select_amount_atm(money){
    $("#noptien_label").text('');
    $("#input_topup_atm").val(money);
}

function get_provider(ProdType){
    $("#provider-list-home").html('');
    $.post("/get-provider", {ProdType: ProdType},
        function (res, status) {
            $("#provider-list-home").html(res);
        });
}

function changeNumber(number){
    var newNumber = parseInt(number) + parseInt($(".quantity-value").val());
    var quantity = newNumber;
    if(quantity == 1){
        document.getElementById('numberDown').setAttribute("disabled", "disabled");
        document.getElementById('numberDown').setAttribute("style", "");
        $("#numberUp").removeAttr("disabled");
    }
    if(quantity > 1 && quantity < 50){
        $("#numberDown").removeAttr("disabled");
        $("#numberUp").removeAttr("disabled");
        document.getElementById('numberUp').setAttribute("style", "background-color:rgb(170, 170, 170); border-color: rgb(170, 170, 170);");
        document.getElementById('numberDown').setAttribute("style", "background-color:rgb(170, 170, 170); border-color: rgb(170, 170, 170);");
    }
    if(quantity == 50){
        document.getElementById('numberUp').setAttribute("disabled", "disabled");
        document.getElementById('numberUp').setAttribute("style", "");
    }
    if(newNumber > 50){
        newNumber = 50;
    }
    $(".quantity-value").val(newNumber);
    creat_bill_pay();
}

function creat_bill_pay(){
    var charge_service = $(".payment-list .selected").data('transfee');
    var newNumber = parseInt($(".quantity-value").val());
    var card_sale = $("#lis-item-card .selected").data('sale') * newNumber;
    var rate = $(".payment-list .selected").data('rate');
    var pdv = parseInt($("#lis-item-card .selected").data('money') * newNumber * rate) + parseInt(charge_service);
    var total = parseInt($("#lis-item-card .selected").data('money') * newNumber) + parseInt(pdv);
    var card_money = $("#lis-item-card .selected").data('moneyc');

    if(charge_service == 0){
        $('#charge_service').html('').append('Miá»…n phĂ­');
    } else {
        $('#charge_service').html('').append(pdv.toLocaleString('de-DE')+ 'Ä‘');
    }
    $('#number_trans').html('').append(newNumber);
    $('.sale-price .amount').html('').append(card_sale.toLocaleString('de-DE'));
    $('.total-price .amount').html('').append(total.toLocaleString('de-DE'));
    $('.card-money .amount').html('').append(card_money.toLocaleString('de-DE'));
}

function payment_card(){
    var sid = $("#provider-list-home .selected").data('cardid');
    var carId = $("#lis-item-card .selected").data('thecao');
    var methodId = $(".payment-list .selected").data('payid');
    var prdtype = $(".v-tabs-bar__content .v-tab--active").data('prdtype');
    if(!sid){
        $('#dk_alert').modal('show');
        $('#dk_res').text('Báº¡n chÆ°a chá»n nhĂ  cung cáº¥p tháº»');
        return false;
    }
    if(!methodId){
        $('#dk_alert').modal('show');
        $('#dk_res').text('Báº¡n chÆ°a chá»n hĂ¬nh thá»©c thanh toĂ¡n');
        return false;
    }
    if(!carId){
        $('#dk_alert').modal('show');
        $('#dk_res').text('Báº¡n chÆ°a chá»n má»‡nh giĂ¡ tháº»');
        return false;
    }
    var email = $('#input-email-by').val();
    if(!email){
        $('#dk_alert').modal('show');
        $('#dk_res').text('Báº¡n chÆ°a nháº­p Ä‘á»‹a chá»‰ email Ä‘á»ƒ nháº­n thĂ´ng tin tháº»');
        $('#input-email-by').focus();
        return false;
    }
    if(prdtype == 3 || prdtype == 4 ){
        if(Isphone(email)==false){
            $('#dk_alert').modal('show');
            $('#dk_res').text('Sá»‘ Ä‘iá»‡n thoáº¡i khĂ´ng Ä‘Ăºng Ä‘á»‹nh dáº¡ng');
            $('#input-email-by').focus();
            return false;
        }
    } else {
        if(IsEmail(email)==false){
            $('#dk_alert').modal('show');
            $('#dk_res').text('Email khĂ´ng Ä‘Ăºng Ä‘á»‹nh dáº¡ng');
            $('#input-email-by').focus();
            return false;
        }
    }

    $('#email_receive').text($('#input-email-by').val());
    $('#confirm_pay').modal('show');
//    access_pay();
}

function access_pay(){
    var methodId = $('input[name="pay_type"]:checked').val();
    var full_name = $("#full_name").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var diachi = $("#diachi").val();
    if(!full_name || !email || !phone || !diachi){
        $('#noti_payment').removeClass('alert-info');;
        $('#noti_payment').addClass('alert-danger');
    }
    if(!full_name){
        $('#noti_payment').text('ChÆ°a nháº­p há» tĂªn');
        return false;
    }
    if(!email){
        $('#noti_payment').text('ChÆ°a nháº­p email');
        return false;
    }
    if(!phone){
        $('#noti_payment').text('ChÆ°a nháº­p sá»‘ Ä‘iá»‡n thoáº¡i');
        return false;
    }
    if(!diachi){
        alert('ChÆ°a nháº­p Ä‘á»‹a chá»‰');
        return false;
    }
    if(!methodId){
        alert('Báº¡n chÆ°a chá»n hĂ¬nh thá»©c thanh toĂ¡n');
        return false;
    }
    if(methodId == 1){
        var url = '/payment-process';
    }else if(methodId == 12 || methodId == 9 || methodId == 10 || methodId == 11){
        var url = '/payment-vnpay';
    }
    else if(methodId == 8){
        var url = '/payment-wl';
    } else {
        var url = '/payment-e';
    }

    $.post(url, {phone: phone,methodId: methodId,email: email, full_name: full_name,diachi: diachi},
        function (kq, status) {
            var data = jQuery.parseJSON(kq);
            if(data.error == 0){
                $('#confirm_pay').modal('hide');
                if(methodId == 12 || methodId == 9 || methodId == 10 || methodId == 11){
                    var res = data.data;
                    const a = document.getElementById("open-link");
                    a.setAttribute('onClick', window.open(res,'_blank').focus());
                }else {
                    var res = data.data;
                    const a = document.getElementById("open-link");
                    a.setAttribute('onClick', window.open(res,'_top').focus());
                }

            } else {
                $('#dk_alert').modal('show');
                $('#dk_res').text(data.msg);
            }
        });
}

function wallet_pay(){
    var full_name = $("#full_name").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var diachi = $("#diachi").val();
    if(!full_name || !email || !phone || !diachi){
        $('#noti_payment').removeClass('alert-info');;
        $('#noti_payment').addClass('alert-danger');
    }
    if(!full_name){
        $('#noti_payment').text('ChÆ°a nháº­p há» tĂªn');
        return false;
    }
    if(!email){
        $('#noti_payment').text('ChÆ°a nháº­p email');
        return false;
    }
    if(!phone){
        $('#noti_payment').text('ChÆ°a nháº­p sá»‘ Ä‘iá»‡n thoáº¡i');
        return false;
    }
    if(!diachi){
        $('#noti_payment').text('ChÆ°a nháº­p Ä‘á»‹a chá»‰');
        return false;
    }
    var url = '/payment-wallet';
    $.post(url, {full_name: full_name,phone: phone,email: email, diachi: diachi},
        function (kq, status) {
            var data = jQuery.parseJSON(kq);
            if(data.error == 0){
                var res = data.data;
                const a = document.getElementById("open-link");
                a.setAttribute('onClick', window.open(res,'_top').focus());
            } else {
                $('#noti_payment').removeClass('alert-info');;
                $('#noti_payment').addClass('alert-danger');
                $('#noti_payment').text(data.msg);
            }
        });
}

function openInfoProduct(transId){
    $.get("/info-trans", {transId: transId},
        function (res, status) {
            var data = jQuery.parseJSON(res);
            if(data.error == 0){
                $('#info-trans').modal('show');
                $('#startDate').text(data.data.startDate);
                $('#endDate').text(data.data.endDate);
                $('#iccid').text(data.data.iccid);
                $('#orderNo').text(data.data.orderNo);
                $('#downloadUrl').text(data.data.downloadUrl);
                $('#orderStatus').text(data.data.orderStatus);
                $('#img-qrcode').html('<img src="'+data.data.qrcode+'" class="img-fit" >');
            }
        });
}

function isNumeric(value) {
    return /^-?\d+$/.test(value);
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!regex.test(email)) {
        return false;
    } else {
        return true;
    }
}

function Isphone(mobile){
    var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
    if(mobile !==''){
        if (vnf_regex.test(mobile) == false)
        {
            return false;
        }
    }else{
        return false;
    }
}

function get_captcha(type = 0){
    $.ajax({
        type: "POST",
        url: "/get-captcha",
        success: function(data) {
            var res = jQuery.parseJSON(data);
            if(type == 2){
                $("#text-captcha-atm").attr('src',res.path);
            }else if(type == 3){
                $("#text-captcha-vietqr").attr('src',res.path);
            }
            else {
                $("#text-captcha").attr('src',res.path);
            }
        }
    });
}

function checked_package(id){
    $("#title_price").text($("#pricepackage" + id).text());
    jQuery.ajax({
        url: "/ajax/get_prd_by_id/",
        type: "POST",
        data: {id:id},
        timeout: 5000,
        error: function(xhr, status) {
            switch (status) {
                case "timeout":
                    break;

                default:
                    break;
            }
        }, success: function(response) {
            $("#product_detail").html(response);
        }
    });
}
function count_quantity(number){
    var old = $("#quantity").val();
    if(!old){
        old = 1;
    }
    var number_new = parseInt(old)+ parseInt(number);
    if(number_new > 0){
        $("#quantity").val(number_new);
    }
}

function openLink(link){
    const a = document.getElementById("open-link");
    a.setAttribute('onClick', window.open('/'+link,'_top').focus());
}

function addToCat(){

    var pid = $("input[type='radio'][name='attr']:checked").val();
    var quantity = $('#quantity').val();
    jQuery.ajax({
        dataType: "json",
        url: "/gio-hang/add-to-cart/" + pid,
        type: "POST",
        data: {quantity: quantity},
        timeout: 5000,
        error: function(xhr, status) {
            switch (status) {
                case "timeout":
                    break;
                default:
                    break;
            }
        }, success: function(response) {
            if(response.error == 0){
                $("#cat_number").text(response.msg);
            }
        }
    });
}

function payNow(){
    var pid = $("input[type='radio'][name='attr']:checked").val();
    var quantity = $('#quantity').val();
    jQuery.ajax({
        dataType: "json",
        url: "/gio-hang/add-to-cart/" + pid,
        type: "POST",
        data: {quantity: quantity},
        timeout: 5000,
        error: function(xhr, status) {
            switch (status) {
                case "timeout":
                    break;
                default:
                    break;
            }
        }, success: function(response) {
            if(response.error == 0){
                const a = document.getElementById("open-link");
                a.setAttribute('onClick', window.open('/gio-hang','_top').focus());
            }
        }
    });
}

function update_cart(pid, number = 0){
    var quantity = $('#quantity-'+pid).val();
    if(number != 0){
        var quantity = parseInt(quantity)+ parseInt(number);
    }
    $('#quantity-'+pid).val(quantity);
    jQuery.ajax({
        dataType: "json",
        url: "/gio-hang/add-to-cart/" + pid,
        type: "POST",
        data: {quantity: quantity, update : 1},
        timeout: 5000,
        error: function(xhr, status) {
            switch (status) {
                case "timeout":
                    break;
                default:
                    break;
            }
        }, success: function(response) {
            if(response.error == 0){
                location.reload();
            }
        }
    });
}

function delete_cart(id){
    if(!confirm("Báº¡n cĂ³ muá»‘n xĂ³a sáº£n pháº©m khá»i giá» hĂ ng?")) {
        return false;
    }
    jQuery.ajax({
        dataType: "json",
        url: "/gio-hang/remover-item/" + id,
        type: "POST",
        data: {},
        timeout: 5000,
        error: function(xhr, status) {
            switch (status) {
                case "timeout":
                    break;

                default:
                    break;
            }
        }, success: function(response) {
            if(response.error == 0){
                location.reload();
            }
        }
    });
}

function close_modal(){
    $("#info-trans").modal('hide');
    $("#showPaymentType").modal('hide');
}

function showAllCountry(number){
    jQuery.ajax({
        url: "/ajax/show_country/",
        type: "POST",
        data: {number:number},
        timeout: 5000,
        error: function(xhr, status) {
            switch (status) {
                case "timeout":
                    break;

                default:
                    break;
            }
        }, success: function(response) {
            $("#tab_countrys").html(response);
            var scrollDiv = document.getElementById("tab_countrys").offsetTop;
            window.scrollTo({ top: scrollDiv, behavior: 'smooth'});
        }
    });
}

function suggest(inputString) {
    if (inputString.length == 0) {
//        $('#sugges_search').fadeOut();
        $('#suggestionsList').fadeOut();
    } else {
        $.ajax({
            url: "/ajax/auto_sugget",
            data: 'country=' + inputString,
            success: function(msg) {
                if (msg.length > 0) {
                    $('#sugges_search').fadeIn();
                    $('#suggestionsList').fadeIn();
                    $('#suggestionsList').html(msg);
                }
            }
        });
    }
}
